class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

class BST:
    def __init__(self):
        self.root = None
       
    def insert(self, val):
        self.root = self.insertNode(self.root, val)

    def isSimilar(self, otherBST):
        return self.isSimilarBST(self.root, otherBST.root)

    def insertNode(self, root, val):
        if root is None:
            return Node(val)
        if root.val > val:
            root.left = self.insertNode(root.left, val)
        else:
            root.right = self.insertNode(root.right, val)
        return root
            
     def isSimilarBST(self, root1, root2):
        if root1 is None and root2 is None:
            return True
        if root1 is None:
            return False  
        if root2 is None:
            return False
        isLeftSubtreeSimilar = self.isSimilarBST(root1.left, root2.left)
        isRightSubtreeSimilar = self.isSimilarBST(root1.right, root2.right)
        return isLeftSubtreeSimilar and isRightSubtreeSimilar

if __name__=="__main__":
    n, k = map(int, input().split())
    bstMaps = {}
    for i in range(n):
        levels = list(map(int, input().split()))
        bst = BST()
        for level in levels:
            bst.insert(level)
        isUnique = True
        for bstId in bstMaps:
            if bstMaps [bstId].isSimilar(bst):
                isUnique = False
                break
        if isUnique:
            bstMaps[i] = bst
    print(len(bstMaps))


       
